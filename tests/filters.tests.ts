/// <reference path="../typings/tsd.d.ts" />
/// <reference path="../src/app.ts" />
/// <reference path="../src/controllers.ts" />
/// <reference path="../src/filters.ts" />
/// <reference path="../src/models.ts" />
/// <reference path="../src/services.ts" />

describe("Filters", function() {
    describe("RoundFilter", function(){
        it("name of filter", function() {
            expect(Filters.RoundFilter.FILTER_NAME).toBe("round");
        });

        it("filter works", function() {
            expect(Filters.RoundFilter.filter("105.2365")).toEqual(105);
        });
    });

    describe("BornYearFilter", function(){
        it("name of filter", function() {
            expect(Filters.BornYearFilter.FILTER_NAME).toBe("born");
        });

        it("filter works", function() {
            expect(Filters.BornYearFilter.filter("17")).toEqual(2000);
        });
    });
});