/// <reference path="../typings/tsd.d.ts" />
module Filters{
    export class RoundFilter{
        public static FILTER_NAME = "round";        
        public static filter(input: string): number {
            return Math.round(parseFloat(input));
        }
    }

    export class BornYearFilter{
        public static FILTER_NAME = "born";        
        public static filter(input: string): number {
            return new Date().getFullYear() - parseInt(input);
        }
    }

    export function getFilterFactoryFunction(fn: Function): () => Function {
        return function () { return fn; };
    }
}

angular.module('starter.filters', [])

.filter(Filters.RoundFilter.FILTER_NAME, Filters.getFilterFactoryFunction(Filters.RoundFilter.filter))

.filter(Filters.BornYearFilter.FILTER_NAME, Filters.getFilterFactoryFunction(Filters.BornYearFilter.filter));