/// <reference path="../typings/tsd.d.ts" />

module Services{

  export const GNOMES_URL = "https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json";

  export interface IGnomesService{
    fetchData(): ng.IPromise<Models.Gnome[]>;
    pushData(gnomes: Models.Gnome[]): void;
    getGnome(gnomeid: string): Models.Gnome;
  }

  export class Gnomes implements IGnomesService{
    static $inject = ['$http'];
    public gnomes: Models.Gnome[] = null;

    constructor(private $http: ng.IHttpService){}

    public fetchData(): ng.IPromise<Models.Gnome[]>{
      if (this.gnomes !== null){
        return this.gnomes;
      }
      return this.$http.get(GNOMES_URL);
    }

    public pushData(gnomes: Models.Gnome[]): void{
      this.gnomes = gnomes;
    }

    public getGnome(gnomeid: string): Models.Gnome{
      if (this.gnomes !== null){
        for(let i = 0; i < this.gnomes.length; i++) {
          if (this.gnomes[i].id == parseInt(gnomeid)){
            return  this.gnomes[i];
          }
        }
      }
      return null;
    }
  }
}

angular.module('starter.services', [])

.service('Gnomes', Services.Gnomes);