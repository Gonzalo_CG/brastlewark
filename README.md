1. Install Node.js
2. Install bower, tsd, gulp and karma
  $ npm install -g bower
  $ npm install -g tsd
  $ npm install -g gulp
  $ npm install -g karma
  $ npm install -g karma-cli
3. Install dependencies
  $ npm install
  $ bower install
  $ tsd install
4. Go to root folder and run App
  $ gulp run
5. Run tests
  $ karma start