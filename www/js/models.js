var Models;
(function (Models) {
    var Gnome = (function () {
        function Gnome(id, name, thumbnail, age, weight, height, hair_color, professions, friends) {
            this.id = id;
            this.name = name;
            this.thumbnail = thumbnail;
            this.age = age;
            this.weight = weight;
            this.height = height;
            this.hair_color = hair_color;
            this.professions = professions;
            this.friends = friends;
        }
        return Gnome;
    }());
    Models.Gnome = Gnome;
})(Models || (Models = {}));
