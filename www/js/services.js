/// <reference path="../typings/tsd.d.ts" />
var Services;
(function (Services) {
    Services.GNOMES_URL = "https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json";
    var Gnomes = (function () {
        function Gnomes($http) {
            this.$http = $http;
            this.gnomes = null;
        }
        Gnomes.prototype.fetchData = function () {
            if (this.gnomes !== null) {
                return this.gnomes;
            }
            return this.$http.get(Services.GNOMES_URL);
        };
        Gnomes.prototype.pushData = function (gnomes) {
            this.gnomes = gnomes;
        };
        Gnomes.prototype.getGnome = function (gnomeid) {
            if (this.gnomes !== null) {
                for (var i = 0; i < this.gnomes.length; i++) {
                    if (this.gnomes[i].id == parseInt(gnomeid)) {
                        return this.gnomes[i];
                    }
                }
            }
            return null;
        };
        return Gnomes;
    }());
    Gnomes.$inject = ['$http'];
    Services.Gnomes = Gnomes;
})(Services || (Services = {}));
angular.module('starter.services', [])
    .service('Gnomes', Services.Gnomes);
