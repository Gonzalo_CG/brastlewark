/// <reference path="../typings/tsd.d.ts" />
var Filters;
(function (Filters) {
    var RoundFilter = (function () {
        function RoundFilter() {
        }
        RoundFilter.filter = function (input) {
            return Math.round(parseFloat(input));
        };
        return RoundFilter;
    }());
    RoundFilter.FILTER_NAME = "round";
    Filters.RoundFilter = RoundFilter;
    var BornYearFilter = (function () {
        function BornYearFilter() {
        }
        BornYearFilter.filter = function (input) {
            return new Date().getFullYear() - parseInt(input);
        };
        return BornYearFilter;
    }());
    BornYearFilter.FILTER_NAME = "born";
    Filters.BornYearFilter = BornYearFilter;
    function getFilterFactoryFunction(fn) {
        return function () { return fn; };
    }
    Filters.getFilterFactoryFunction = getFilterFactoryFunction;
})(Filters || (Filters = {}));
angular.module('starter.filters', [])
    .filter(Filters.RoundFilter.FILTER_NAME, Filters.getFilterFactoryFunction(Filters.RoundFilter.filter))
    .filter(Filters.BornYearFilter.FILTER_NAME, Filters.getFilterFactoryFunction(Filters.BornYearFilter.filter));
