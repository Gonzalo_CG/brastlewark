/// <reference path="../typings/tsd.d.ts" />
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'starter.filters'])
    .run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            window.StatusBar.styleLightContent();
        }
    });
})
    .config(function ($stateProvider, $urlRouterProvider) {
    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider
        .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'templates/tabs.html'
    })
        .state('tab.home', {
        url: '/home',
        views: {
            'tab-home': {
                templateUrl: 'templates/tab-home.html'
            }
        }
    })
        .state('tab.gnomes', {
        url: '/gnomes',
        views: {
            'tab-gnomes': {
                templateUrl: 'templates/tab-gnomes.html',
                controller: 'GnomesCtrl as vm'
            }
        }
    })
        .state('tab.gnomes-detail', {
        url: '/gnomes/:gnomeid',
        views: {
            'tab-gnomes': {
                templateUrl: 'templates/gnomes-detail.html',
                controller: 'GnomesDetailCtrl as vm'
            }
        }
    });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/tab/gnomes');
});
